# Planning Tutorial #

This is a tutorial repository for setting up a planning module. This short tutorial will walk you through the requisite steps for setting up a workspace with required packages. We will then go over an example. Finally, we will go over some exercises. 

There are two tutorials:

1. Basic
2. Advanced

# Basic Tutorial #

### Prerequisites ###

This tutorial assumes you are working with Ubuntu 14.04 + Indigo. 

We will need CGAL. Install it as follows
```bash
sudo apt-get install libcgal-dev
sudo apt-get install libcgal-qt5-dev
```

### Installation ###

Let's begin by creating a workspace. Go to the directory containing your workspaces, e.g. `cd ~/ros/`. We will do this by using the `.rosinstall` in this repository `planning_tutorial_simple.rosinstall`

```bash
sudo apt-get install python-wstool # get wstool if you dont have it already
mkdir -p planning_tutorial_basic_ws/src
cd planning_tutorial_basic_ws/src
catkin_init_workspace
git clone git@bitbucket.org:castacks/planning_tutorial.git
ln -s planning_tutorial/planning_tutorial_simple.rosinstall .rosinstall
wstool info # see status
wstool up # get stuff
cd ..
catkin_make
```

### Executing the example###

We have an example in `examples/example_simple_r3.cpp` that we will go over in a bit. Since everything compiled, let's try to execute the example.

Open a terminal, start roscore, run rviz and load the config file
```bash
source devel/setup.bash
roscore &
rosrun rviz rviz
# Load the config from planning_tutorial/example_simple_r3.rviz
```

Open another terminal and run the executable
```bash
rosrun planning_tutorial planning_tutorial_example_simple_r3
```

You should see a planner plan from a start to a goal while avoiding red obstacles. The green path is the planned path and the blue graph is the search tree. The planner is repeatedly invoked, and being stochastic, it continues to find a different path with a different cost.

![basic_tutorial_small.png](https://bitbucket.org/repo/7ErKX4K/images/3815994889-basic_tutorial_small.png)

### Code Walkthrough ###

Lets quickly walk through the code in `examples/example_simple_r3.cpp`. 

We are doing geometric planning in R3 space. Our world is a unit box which we setup as follows
```
#!C++

  // Create R3 space
  ob::StateSpacePtr space(new ob::RealVectorStateSpace(3));
  ob::RealVectorBounds bounds(3);  // We are going to create a unit box from (-1,-1,-1) to (1,1,1)
  bounds.setLow(0, -1.0);
  bounds.setLow(1, -1.0);
  bounds.setLow(2, -1.0);
  bounds.setHigh(0, 1.0);
  bounds.setHigh(1, 1.0);
  bounds.setHigh(2, 1.0);
  space->as<ob::RealVectorStateSpace>()->setBounds(bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
```

We are planning from a 3d start configuration to goal configuration

```
#!C++

  // Set start and goal
  ob::ScopedState<ob::RealVectorStateSpace> start(si), goal(si); // Planning from (0,0,0) to (0.8, 0.8, 0.8)
  start->values[0] = 0;
  start->values[1] = 0;
  start->values[2] = 0;
  goal->values[0] = 0.8;
  goal->values[1] = 0.8;
  goal->values[2] = 0.8;
```

Let's create some obstacles. We will represent them as analytic shapes. Let's create a cuboid and a cylinder.

```
#!C++

  // Create some obstacles
  ShapeSet obstacle_set;
  obstacle_set.AddShape(boost::shared_ptr<Shape>(new Cuboid(0.4, 0.3, 0.4, 1.6, -1.1, 1.1))); // x_com,  y_com,  x_size,  y_size,  z_lower,  z_upper
  obstacle_set.AddShape(boost::shared_ptr<Shape>(new Cylinder(0.8, 0.4, 0.35, 0.2, 1.0))); //  x_com, y_com, z_com, radius, height
```

Now we have to tell the planner how to collision check. Geometric planners simply need to know if a state is valid or not. We provide a function handle that determines this. 

```
#!C++

  // Tell the planner how to collision check by creating a function handle that takes a state as input and returns feasible or not
  boost::function<bool(const ob::State*)> valid_fn = [&](const ob::State* s) {
    Eigen::Vector3d pos = planning_common::workspace_utils::GetTranslationVector3d(si, s); // Extract 3d workspace information (would work for non R3 spaces too!)
    return !obstacle_set.InShapeSet(pos);
  };
  si->setStateValidityChecker(valid_fn); 
  si->setStateValidityCheckingResolution(0.001); // dimensionless number as a fraction of the workspace size
  si->setup();
```

Geometric planners minimize path length generally. We can always make new cost functions, but lets use this for now

```
#!C++

  // Setup problem
  ob::ProblemDefinitionPtr pdef(new ob::ProblemDefinition(si));
  pdef->setStartAndGoalStates(start.get(), goal.get());
  pdef->setOptimizationObjective(ob::OptimizationObjectivePtr(new ob::PathLengthOptimizationObjective(si)));
```

We choose the textbook RRT* planner. Don't worry, we will do better eventually.

```
#!C++

  boost::shared_ptr<ompl::geometric::RRTstar> rrtstar(new ompl::geometric::RRTstar(si));
  ob::PlannerPtr planner = rrtstar;
```

The rest of the code invokes the planner in a loop and prints how it did + visualizes the graph.

### Excercises ###

**Excercise 1**
Let's run the RRT* planner. Execute `rosrun planning_tutorial planning_tutorial_example_simple_r3` and observe the terminal to see the cost 

```
#!bash

[ INFO] [1518482962.176641952]: Found solution with cost: 2.72759
[ INFO] [1518482962.969069014]: Found solution with cost: 2.75717
[ INFO] [1518482963.759048751]: Found solution with cost: 2.78773
[ INFO] [1518482964.544764957]: Found solution with cost: 2.68894
[ INFO] [1518482965.333401891]: Found solution with cost: 2.80764
[ INFO] [1518482966.117662200]: Found solution with cost: 2.72583
[ INFO] [1518482966.915902301]: Found solution with cost: 2.66969
[ INFO] [1518482967.704475336]: Found solution with cost: 2.65516

```

**Excercise 2**
Okay lets try a more crazy planner - RRT-Connect. 

We already included this in the headers, you can verify

```
#!C++

#include "ompl/geometric/planners/rrt/RRTConnect.h"
```

You have to replace the RRT* planner with this as shown.

```
#!C++
  // Setup planner
//  boost::shared_ptr<ompl::geometric::RRTstar> rrtstar(new ompl::geometric::RRTstar(si));
//  ob::PlannerPtr planner = rrtstar;
  boost::shared_ptr<ompl::geometric::RRTConnect> rrtconnect(new ompl::geometric::RRTConnect(si));
  ob::PlannerPtr planner = rrtconnect;
```

Observe the cost. It fluctuates a lot! That's because RRT-Connect is a feasible planner - it terminates on finding any feasible path. But it is very fast as it aggressively runs a bi-directional search to connect to every sample. 

**Excercise 3**
Let's go back to RRT* first

```
#!C++

  // Setup planner
  boost::shared_ptr<ompl::geometric::RRTstar> rrtstar(new ompl::geometric::RRTstar(si));
  ob::PlannerPtr planner = rrtstar;
```

Now let's change the obstacles a bit to create a narrow passage

```
#!C++

  // Create some obstacles
  ShapeSet obstacle_set;
//  obstacle_set.AddShape(boost::shared_ptr<Shape>(new Cuboid(0.4, 0.3, 0.4, 1.6, -1.1, 1.1)));
//  obstacle_set.AddShape(boost::shared_ptr<Shape>(new Cylinder(0.8, 0.4, 0.35, 0.2, 1.0)));
  obstacle_set.AddShape(boost::shared_ptr<Shape>(new Cuboid(0.4, 0.5, 0.4, 0.8, -1.1, 1.1)));
  obstacle_set.AddShape(boost::shared_ptr<Shape>(new Cylinder(0.81, 0.4, 0.35, 0.2, 1.1)));
```

Execute the code. Notice that RRT* is not able to find the gap (not frequently at least).

** Excercise 4 **

Let's run a more state-of-the-art planner: BIT*. 
We already included this in the headers, you can verify

```
#!C++

#include "ompl/geometric/planners/bitstar/BITstar.h"
```

Let's change the planner

```
#!C++

  // Setup planner
//  boost::shared_ptr<ompl::geometric::RRTstar> rrtstar(new ompl::geometric::RRTstar(si));
  //  ob::PlannerPtr planner = rrtstar;
  boost::shared_ptr<ompl::geometric::BITstar> bitstar(new ompl::geometric::BITstar(si));
  bitstar->setSamplesPerBatch(100);
  ob::PlannerPtr planner = bitstar;
```

Build and execute. You should see this:

![bitstar_small.png](https://bitbucket.org/repo/7ErKX4K/images/4248990615-bitstar_small.png)

This is because BITstar does two important tricks: 1. It does informed sampling, i.e. samples directly in places where the solution can potentially improve the cost 2. It uses incremental lazy Astar - that means it judiciously decides which samples to process.

# Advanced Tutorial #

### Prerequisites ###

This tutorial assumes you are working with Ubuntu 14.04 + Indigo. 

We will need CGAL and NLOPT. Install them as follows
```bash
sudo apt-get install libcgal-dev
sudo apt-get install libnlopt-dev
```

### Installation ###
For this advanced tutorial, we will use the advanced branch of this package. Let's begin by creating a workspace. Go to the directory containing your workspaces, e.g. `cd ~/ros/`. Clone the repository and switch branches
```
sudo apt-get install python-wstool # get wstool if you dont have it already
mkdir -p planning_tutorial_advanced_ws/src
cd planning_tutorial_advanced_ws/src
catkin_init_workspace
git clone git@bitbucket.org:castacks/planning_tutorial.git
cd planning_tutorial
git checkout advanced
cd ..
ln -s planning_tutorial/planning_tutorial_advanced.rosinstall .rosinstall
wstool info # see status
wstool up # get stuff
cd ..
catkin_make
```

### Executing the example###

We have an example in `examples/example_advanced_dubinsz.cpp` that we will go over in a bit. Since everything compiled, let's try to execute the example. 

Open a terminal, start roscore, run rviz and load the config file
```bash
roscore &
rosrun rviz rviz
# Load the config from planning_tutorial/example_advanced_dubinsz.rviz
```

Open another terminal and run the launch file 
```bash
roslaunch planning_tutorial example_advanced_dubinsz.launch
```
You should see an environment with hills, wires and no-fly zone. You should see a planner plan from a start to a goal while avoiding red obstacles. The green path is the planned path and the blue graph is the search tree. The planner is repeatedly invoked it continues to find a different path with a different cost.

![advanced_tutorial_small.png](https://bitbucket.org/repo/7ErKX4K/images/2626118548-advanced_tutorial_small.png)

### Code walkthrough ###

Let's quickly walk through the code in 'examples/example_advanced_dubinsz.cpp'. 

The overall planning problem is to produce a path in 4D (x,y,z, heading) from a start configuration to a goal. There are some constraints on the path: a minimum radius constraint and a maximum glide slope constraints. The path must avoid all terrain obstacles (the occupancy grid) by a clearance. The path must also not intersect with some geometric shapes. Finally, it must lie inside a valid volume.

The problem is specified as follows:

```
#!C++

  // Specify the problem, constraints, start and goal
  ob::SpaceInformationPtr si_xyzpsi = xsu::GetStandardXYZPsiSpacePtr();
  ob::ScopedState<XYZPsiStateSpace> start(si_xyzpsi), goal(si_xyzpsi);
  start->SetXYZ(Eigen::Vector3d(0,0,0));  // Start (x,y,z,heading)
  start->SetPsi(0);
  goal->SetXYZ(Eigen::Vector3d(200,0,0)); // Goal (x,y,z, heading)
  goal->SetPsi(0);
  UAVPathDynamicsConstraints constraints;
  constraints.set_min_radius(10.0); // minimum radius
  constraints.set_glideslope_neg_z(0.2); // max glideslope for climbing
  constraints.set_glideslope_pos_z(0.2); // max glideslope for descending
  UAVPathProblemParams problem_params;
  problem_params.max_time = 1.0;  // max planning time
  problem_params.spatial_step = 1.0;  // max waypoint separation of path
  problem_params.path_clearance = 4.0; // size of robot (clearance)
```

Next, we create a representation of the world. **Note:** The planning algorithm must not depend on perception packages because perception representation may change from project to project (and we cant keep rewriting the planner). So we use a set of general representations to interact with the planner. The code snippet below shows these representations. Note that we create a scope to lock-in the perception specific code to demonstrate the planner doesn't use it directly

```
#!C++

  boost::shared_ptr<representation_interface::ObstacleGridRepresentationInterface> general_distance_representation; // General representation of occupancy map that the planner uses
  ShapeSet invalid_volume, valid_volume; // a set of shapes that represent valid and invalid volume
  pcl::PointCloud<pcl::PointXYZ>::Ptr visualization_occupancy_map(new pcl::PointCloud<pcl::PointXYZ>()); // for visualizing the environment

  { // Scoping the environment creation to demonstrate that planning deals with general representations

    // We will create a parametric model of the environment
    env::TerrainEnvironment env;
    env.AddGroundPlane(6); // z level for ground (NED convention)
    env.AddHill(Eigen::Vector2d(150, -70), 20, 40); // Add a hill
    env.AddHill(Eigen::Vector2d(70, 50), 20, 40); // Add a hill
    env.AddWire(Eigen::Vector2d(100, -100), Eigen::Vector2d(100, 100), 15, 15, 2); // Add a wire sequence

    // Lets voxelize the environment
    float resolution = 2;
    Eigen::Vector3d origin(0, -100, -37);
    Eigen::Vector3d dimension(200, 200, 45);
    std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > occupied;
    env.VoxelizeEnvironment(origin, dimension, resolution, occupied);

    // We need to make sure we avoid obstacles by the path clearance. Hence we need a distance transform that maps the occupancy map (0 / 1) to a truncated distance map (distance to nearest obstacle).
    boost::shared_ptr<IncrementalDistanceTransform> dt(new IncrementalDistanceTransform());
    dt->initNodes(Eigen::Vector3d(origin[0], origin[1], origin[2]) ,
                  Eigen::Vector3i(dimension[0]/resolution, dimension[1]/resolution, dimension[2]/resolution),
                  resolution,
                  static_cast<unsigned short>((problem_params.path_clearance+resolution)/resolution),
                  1,
                  false);

    boost::shared_ptr<ri::IDTRepresentationInterface> idt_rep(new ri::IDTRepresentationInterface());
    idt_rep->set_dt(dt);
    dt->AddObstacleSet(occupied);
    *visualization_occupancy_map = *dt->GetOccPointCloud();  // for visualization
    general_distance_representation = idt_rep; // copy back to general distance representation

    invalid_volume.AddShape(boost::shared_ptr<Shape>(new Cuboid(150.0, -10.0, 40.0, 50.0, -50.0, 15.0)));  // Add another tower
    valid_volume.AddShape(boost::shared_ptr<Shape>(new Cuboid(100.0, 0.0, 220.0, 200.0, -18.0, 5.0))); // This is the valid volume outside of which planner cant go
  }
```

We now load a planner from the planning library. Check the launch file to see which planner you are loading


```
#!C++

  // Load the UAV path planning library
  UAVPathPlannerLibrary planner_library(si_xyzpsi);
  planner_library.SetDisRep(general_distance_representation);

  // Initialize the requested planner in the parameter (refer to the launch file invoking this node)
  std::string planner_name;
  if (!n.getParam("planner_name", planner_name)) {
    ROS_ERROR_STREAM("Couldnt get planner_name");
    return EXIT_FAILURE;
  }
  UAVPathPlannerWrapperPtr selected_planner;
  if(!planner_library.GetInitializedPlanner(n, planner_name, selected_planner)) {
    ROS_ERROR_STREAM("Couldnt initialize planner");
    return EXIT_FAILURE;
  }
```

### Excercies ###

**Excercise 1**

Let's run the planner `dubinsz_bitstar_planner_1'. Go to the launch file in `launch/example_advanced_dubinsz.launch' and modify it as follows

```
#!XML

<launch>
<!-- Transform from NED to upright for viewing purposes -->
<node pkg="tf" type="static_transform_publisher" name="world_to_world_view"  args="0 0 0 0 3.14159 0 world world_view 100"/>

<node name="example_advanced_dubinsz" pkg="planning_tutorial" type="planning_tutorial_example_advanced_dubinsz" output="screen"> 
  <rosparam file="$(find uav_path_planner_library)/resources/planner_ensemble_dictionary.yaml"/> <!-- the YAML file containing the planner library config -->
  <param name="planner_name" type="string" value="dubinsz_bitstar_planner_1" />  <!-- The planner being invoked -->
</node>

</launch>
```
Execute the launch file. Examine the console and look at the path cost. While the planner finds a path almost always, there is quite a bit of fluctuation in path costs. This is because the BIT* planner is  general purpose, it's not overfitting at all to the scenario and not able to find the global optima in the small time budget.

```
#!bash

[ INFO] [1518464406.556441099]: Found a feasible plan with path length: 335.88
[ INFO] [1518464407.058601860]: Begin planning
[ INFO] [1518464408.148625226]: Found a feasible plan with path length: 281.39
[ INFO] [1518464408.649910914]: Begin planning
[ INFO] [1518464409.733094803]: Found a feasible plan with path length: 312.528
[ INFO] [1518464410.234509936]: Begin planning
[ INFO] [1518464411.322421624]: Found a feasible plan with path length: 333.736
[ INFO] [1518464411.823657290]: Begin planning
[ INFO] [1518464412.904579643]: Found a feasible plan with path length: 415.699
[ INFO] [1518464413.405940144]: Begin planning
[ INFO] [1518464414.487554162]: Found a feasible plan with path length: 284.804
[ INFO] [1518464414.988732265]: Begin planning
[ INFO] [1518464416.073985929]: Found a feasible plan with path length: 254.633
[ INFO] [1518464416.575328270]: Begin planning
[ INFO] [1518464417.662909231]: Found a feasible plan with path length: 293.383
[ INFO] [1518464418.164285437]: Begin planning
[ INFO] [1518464419.264446827]: Found a feasible plan with path length: 291.299
[ INFO] [1518464419.766479234]: Begin planning
[ INFO] [1518464420.851012259]: Found a feasible plan with path length: 310.665
[ INFO] [1518464421.352545459]: Begin planning
[ INFO] [1518464422.443805129]: Found a feasible plan with path length: 339.384

```

**Excercise 2**

Let's run the planner `dubinsz_rrtstar_tunnel_planner_3'. Go to the launch file in `launch/example_advanced_dubinsz.launch' and modify it as follows

```
#!XML

<launch>
<!-- Transform from NED to upright for viewing purposes -->
<node pkg="tf" type="static_transform_publisher" name="world_to_world_view"  args="0 0 0 0 3.14159 0 world world_view 100"/>

<node name="example_advanced_dubinsz" pkg="planning_tutorial" type="planning_tutorial_example_advanced_dubinsz" output="screen"> 
  <rosparam file="$(find uav_path_planner_library)/resources/planner_ensemble_dictionary.yaml"/> <!-- the YAML file containing the planner library config -->
  <param name="planner_name" type="string" value="dubinsz_rrtstar_tunnel_planner_3" />  <!-- The planner being invoked -->
</node>

</launch>
```
Execute the launch file. Examine the console and look at the path cost. Notice that this planner has much lower cost (although occasionally does not find a path). This is because the RRT*Tunnel planner focuses its sampling in a tunnel (+samples in workspace). It does well for scenarios where a path is in a tunnel around the straight line joining start and goal. Think of it as a precision planner that is designed to overfit on problems such as these.


```
#!bash

[ INFO] [1518464637.949655396]: Begin planning
[ INFO] [1518464639.016201271]: No solution found
[ INFO] [1518464639.517403688]: Begin planning
[ INFO] [1518464640.585617680]: Found a feasible plan with path length: 235.803
[ INFO] [1518464641.087000200]: Begin planning
[ INFO] [1518464642.150959057]: Found a feasible plan with path length: 233.064
[ INFO] [1518464642.652232449]: Begin planning
[ INFO] [1518464643.718752514]: Found a feasible plan with path length: 239.952
[ INFO] [1518464644.219949798]: Begin planning
[ INFO] [1518464645.285023583]: Found a feasible plan with path length: 272.57
[ INFO] [1518464645.786332056]: Begin planning
[ INFO] [1518464646.856136342]: Found a feasible plan with path length: 243.969
[ INFO] [1518464647.357565336]: Begin planning
[ INFO] [1518464648.420387737]: Found a feasible plan with path length: 241.326
[ INFO] [1518464648.921393840]: Begin planning
[ INFO] [1518464649.984969691]: Found a feasible plan with path length: 230.559
[ INFO] [1518464650.485909551]: Begin planning
[ INFO] [1518464651.549987648]: Found a feasible plan with path length: 249.059
[ INFO] [1518464652.051208629]: Begin planning
[ INFO] [1518464653.116305074]: Found a feasible plan with path length: 262.632
[ INFO] [1518464653.617436114]: Begin planning
[ INFO] [1518464654.689795915]: Found a feasible plan with path length: 277.608

```

**Excercise 3**

Let's run the planner `dubinsz_astar_planner_5'. Go to the launch file in `launch/example_advanced_dubinsz.launch' and modify it as follows


```
#!XML

<launch>
<!-- Transform from NED to upright for viewing purposes -->
<node pkg="tf" type="static_transform_publisher" name="world_to_world_view"  args="0 0 0 0 3.14159 0 world world_view 100"/>

<node name="example_advanced_dubinsz" pkg="planning_tutorial" type="planning_tutorial_example_advanced_dubinsz" output="screen"> 
  <rosparam file="$(find uav_path_planner_library)/resources/planner_ensemble_dictionary.yaml"/> <!-- the YAML file containing the planner library config -->
  <param name="planner_name" type="string" value="dubinsz_astar_planner_5" />  <!-- The planner being invoked -->
</node>

</launch>
```

Execute the launch file. Examine the console and look at the path cost. Isn't lattice search amazing when it works! And since its discrete search (with some randomized smoothing) it is very consistent. However, it can be brittle - we got lucky that there just happened to be a path on this lattice. It is only resolution complete.

![astar_magic_small.png](https://bitbucket.org/repo/7ErKX4K/images/3962219656-astar_magic_small.png)


```
#!bash

[ INFO] [1518465064.122474008]: Found a feasible plan with path length: 270.281
[ INFO] [1518465064.623076593]: Begin planning
[ INFO] [1518465064.720488181]: Found a feasible plan with path length: 270.267
[ INFO] [1518465065.221180816]: Begin planning
[ INFO] [1518465065.322247751]: Found a feasible plan with path length: 259.824
[ INFO] [1518465065.823005299]: Begin planning
[ INFO] [1518465065.928590918]: Found a feasible plan with path length: 275.766
[ INFO] [1518465066.429084617]: Begin planning
[ INFO] [1518465066.523514533]: Found a feasible plan with path length: 265.339
[ INFO] [1518465067.024221710]: Begin planning
[ INFO] [1518465067.134969527]: Found a feasible plan with path length: 265.339

```


# General Information #
The purpose of this section is to provide additional information about packages and utils useful for planning **(work in progress)**

The following diagram illustrates the typical pipeline for using motion planning. The diagram illustrates that the key objective is to allow independence between the different components. All components are combined by the application. 

![planning_flow.png](https://bitbucket.org/repo/nRxLBR/images/3497220109-planning_flow.png)


Key components

* **State Spaces**: This inherits from the ompl definition of state spaces. This typically describes the dynamics of your system, and related properties such as distance between states, heuristic distance between states etc. (refer to ompl documentation for more details)
* **Representation Interface**: This contains information about how planning algorithms can query about obstacles / gradients etc
* **Cost functions**: This inherits from ompls generic definition of cost functions. The cost functions work with an abstract representation interface if they are related to obstacle costs etc.
* **Planning Algorithms**: These work with abstract state spaces and abstract cost functions. 
* **Application**: This is the end of the pipeline. It instantiates all the abstract classes by choosing a state space, cost function, planning algorithm. 

We now describe the core packages related to planning.



### Base ###
- [planning_common](https://bitbucket.org/castacks/planning_common): This is a collection of useful structures needed for planning. It builds on top of ompl adding fundamental paths, states, objectives and utils. 
- [representation_interface](https://bitbucket.org/castacks/representation_interface): This links planning and perception. Its a templatized interface that is used by planning cost functions for collision checking, etc. This package is specialized by child packages for custom obstacle representation classes. 
- [common_representation_interface](https://bitbucket.org/castacks/common_representation_interface): It is a specialization of representation interface using standard mapping / distancemap.
- [ompl](https://bitbucket.org/castacks/ompl): This is the mother planning package. It contains base classes such as state spaces, paths and geometric planners.

### Path Planning Algorithms ###
We would recommend using the following wrapper package [uav_path_planner_library](https://bitbucket.org/castacks/uav_path_planner_library). It is a library of planning algorithms that have been used in AACUS, mini-aacus and optnav. It is essentially a set of wrappers that make it easy to use for UAV planning. 

Alternately, you can also look at the following packages for the base algorithms

- [ompl](https://bitbucket.org/castacks/ompl): Contains most of the base planners. The go-to algorithm would be BITstar. 
- [chomp](https://bitbucket.org/castacks/chomp): The covariant gradient descent algorithm. Specialized examples are found in [chomp_examples](https://bitbucket.org/castacks/chomp_examples).
- [sampling_planners](https://bitbucket.org/castacks/sampling_planners): This extends the list of sampling planners in ompl. It also has an RRTstar which is much nicer than ompl's version because it allows workspace sampling. 
Current list of planners: RRTstar, Task space RRTstar, Anytime RRT. Specialized examples are found in [sampling_planners_example](https://bitbucket.org/castacks/sampling_planners_example).
- [state_lattice](git@bitbucket.org:castacks/state_lattice.git): Discrete search on a lattice using heuristics. This is useful for examples where sampling methods don't work well. To see how we can learn heuristics, take a look at [sail_state_lattice](https://bitbucket.org/castacks/sail_state_lattice).
- [spartan](https://bitbucket.org/castacks/spartan): Sparse Tangential Network planner. This is basically an efficient way to do 3D visibility graph planning.

To see overall examples of how to use one or more of the algorithms, you can also take a look at the planner examples package [planner_examples](https://bitbucket.org/castacks/planner_examples).

### State Spaces ###
- [curvature_constrained_state_spaces](https://bitbucket.org/castacks/curvature_constrained_state_spaces): A set of spaces that are characterized by curvature (and derivatives) constraints